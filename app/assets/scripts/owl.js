Edools.events.subscribe('school.home', function() {
  setTimeout(timeout_func, 1000);
});

function timeout_func () {
  var carousel = $('.owl-card-carousel');

  if (carousel.length >= 1 && carousel.find('.owl-card-item').length >= 1) {
    initialize_owl();
  } else {
    setTimeout(timeout_func, 1000)
  };

};

function initialize_owl () {
  var owl = $('.owl-card-carousel').owlCarousel({
    dots: false,
    responsive: {
      0: {
        items: 1,
        slideBy: 1
      },
      992: {
        items: 2,
        slideBy: 2
      },
      1200: {
        items: 3,
        slideBy: 3
      }
    }
  });

  var controls = $('.owl-card-controls');

  controls.find('button').click(function(event) {
    if ($(this).hasClass('active') && $(this).hasClass('owl-next')) {
      owl.trigger('next.owl.carousel');
    } else if ($(this).hasClass('active') && $(this).hasClass('owl-prev')) {
      owl.trigger('prev.owl.carousel');
    };
  });

  owl.on('changed.owl.carousel', function(event) {
    if (event.item.index == 0) {
      controls.find('.owl-prev').removeClass('active')
    } else {
      controls.find('.owl-prev').addClass('active')
    };

    if (event.item.index == (event.item.count - event.page.size)) {
      controls.find('.owl-next').removeClass('active')
    } else {
      controls.find('.owl-next').addClass('active')
    };
  });
};
